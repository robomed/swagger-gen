import moment from "moment";

export interface Versioned<T> {
  version: number;
  data: T;
}

export type VersionedList<T> = Versioned<T[]>;

export interface ApiSuccess<T> {
  type: "success";
  data: T;
}

export interface ApiFailure<E> {
  type: "failure";
  data: E;
}

export type ApiResult<T, E> = ApiSuccess<T> | ApiFailure<E>;

export class HttpError extends Error {
  constructor(public response: Response) {
    super(response.statusText);
  }
}


