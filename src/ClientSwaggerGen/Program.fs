open Microsoft.OpenApi.Models

// Learn more about F# at http://fsharp.org

open System
open Microsoft.Extensions.Configuration
open System.Net.Http
open Microsoft.OpenApi.Readers
open Robomed.ClientSwaggerGen
open System.IO

[<EntryPoint>]
let main argv =
    let config =
        ConfigurationBuilder()
            .AddJsonFile("appSettings.json", optional = false)
            .AddCommandLine(argv)
            .Build();
    let url = config.GetValue<string>("url");
    let readApi url =
        let uri = Uri(url)
        use client = new HttpClient(Timeout = TimeSpan.FromSeconds(30.))
        client.GetStringAsync(uri).Result
    let swagger = readApi url
    let (api, diag) = OpenApiStringReader().Read(swagger);
    
    let types = TypeScriptTypeGenerator.generate api
    let client = TypeScriptClientGenerator.generate api

    File.WriteAllText(config.GetValue<string>("target"), types + client)

    
    0 // return an integer exit code
