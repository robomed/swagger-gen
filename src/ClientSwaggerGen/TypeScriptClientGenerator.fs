module TypeScriptClientGenerator
open System
open Robomed.ClientSwaggerGen.SourceBuilder
open Robomed.ClientSwaggerGen.Patterns
open Microsoft.OpenApi.Models

let normalize (s :string) = s.Replace("[", "").Replace("]", "").Replace(",", "").Replace(" ", "")

type Response = {
    Status: int
    Success: bool
    ReturnType: string;
    IsVoid : bool
}

let rec private getTypeBySchema (schema: OpenApiSchema) =
    let nullable =
        if schema.Nullable then " | null" else ""
    (match schema with
    | IsLocalRef ref -> (normalize ref)
    | IsInteger _ -> "number"
    | IsNumber _ -> "number"
    | IsBoolean _ -> "boolean"
    | StringEnum keys ->
        keys |> Seq.map (fun t -> "\"" + t + "\"") |> String.concat " | " 
    | IsString _ -> "string"
    | Array el -> (getTypeBySchema el) + "[]"
    | _ -> invalidOp "unknown schema") + nullable

let generate (doc: OpenApiDocument) =
    let body = create 2
    let intf = create 2
    let types = create 2


    let generatePath url (path : OpenApiPathItem) =
        let generateOp (verb: OperationType)  (op : OpenApiOperation) =
            let opId = op.OperationId
            let opIdCaps = Char.ToUpper(opId.[0]).ToString() + (opId.Substring(1))
            let paramStr = op.Parameters |> Seq.map (fun p -> sprintf "%s: %s" p.Name (getTypeBySchema p.Schema)) |> String.concat ", "
            let paramStr =
                if op.RequestBody |> isNull |> not && op.RequestBody.Content |> isNull |> not then
                    match op.RequestBody.Content.TryGetValue("application/json") with
                    | (true, v) -> if paramStr <> "" then paramStr + ", request: " + (getTypeBySchema v.Schema) else "request: " + (getTypeBySchema v.Schema)
                    | _ -> paramStr
                else paramStr
            let genResponse (code:int) (resp: OpenApiResponse) =
                match resp.Content.TryGetValue("application/json") with
                | true, mt -> {
                       Status = code
                       Success = code < 300
                       ReturnType = getTypeBySchema mt.Schema
                       IsVoid = false
                    }
                | _ -> {
                       Success = code < 300
                       Status = code
                       ReturnType = "undefined"
                       IsVoid = true
                   }
            let genResponseTypes responses (cnts: Map<bool, int>) =
                let genResponseType resp =
                    if cnts.[resp.Success] = 1 then ()
                    else
                        types.print (sprintf "interface %s%dResponse {" opIdCaps resp.Status)
                        types.nl()
                        types.indent()
                        types.print (sprintf "status: %d;" resp.Status)
                        types.nl()
                        match resp.IsVoid with
                        | false ->
                            resp.ReturnType |> sprintf "data: %s" |> types.print
                            types.nl()
                        | _ -> () 
                        types.unIndent()
                        types.print "};"
                        types.nl()
                let genTypeGroup suffix resps  =
                    match resps with
                    | [] -> ()
                    | [_] -> ()
                    | _ ->
                        resps
                        |> Seq.map (fun p -> sprintf "%s%dResponse" opIdCaps p.Status)
                        |> String.concat " | "
                        |> sprintf "type %s%s = %s;" opIdCaps suffix
                        |> types.print
                        types.nl()
                responses |> List.iter genResponseType
                responses |> List.filter (fun p -> p.Success) |> genTypeGroup "Success"
                responses |> List.filter (fun p -> p.Success |> not) |> genTypeGroup "Failure"
            let responses =
                op.Responses |> Seq.map (fun p -> genResponse (int32 p.Key) p.Value) |> Seq.toList
            let resultType, responseCnts =
                match responses |> List.filter (fun p -> p.Success), responses |> List.filter (fun p -> p.Success |> not) with
                | [], [] -> "void", [(true,0); (false, 0)] |> Map.ofList 
                | [s], [] -> s.ReturnType, [(true,1); (false, 0)] |> Map.ofList 
                | [], [f] -> sprintf "ApiResult<void, %s>" f.ReturnType, [(true,0); (false, 1)] |> Map.ofList 
                | [s], [f] -> sprintf "ApiResult<%s, %s>" s.ReturnType f.ReturnType, [(true,1); (false, 1)] |> Map.ofList 
                | [s], _ -> sprintf "ApiResult<%s, %sFailure>" s.ReturnType opIdCaps, [(true,1); (false, 2)] |> Map.ofList 
                | _, [f] -> sprintf "ApiResult<%sSuccess, %s>" opIdCaps f.ReturnType, [(true,2); (false, 1)] |> Map.ofList 
                | _, _ -> sprintf "ApiResult<%sSuccess, %sFailure>" opIdCaps opIdCaps, [(true,2); (false, 2)] |> Map.ofList 
            genResponseTypes responses responseCnts
                    
            (*types.print (sprintf "type %sResult = %s;" opIdCaps resultType)
            types.nl()
            types.nl()*)

            intf.print (sprintf "%s: (%s) => Promise<%s>;" opId paramStr resultType)
            intf.nl()

            body.print (sprintf "%s: async (%s): Promise<%s> => {" opId paramStr resultType)
            body.nl()

            body.indent()

            body.print (sprintf "const url = \"%s\"" url)
            body.indent()

            // Path parameters
            op.Parameters
            |> Seq.filter (fun p -> p.In.Value = ParameterLocation.Path)
            |> Seq.iter (fun p ->
                    body.nl()
                    body.print (sprintf ".replace(\"{%s}\", String(%s))" p.Name p.Name)
               )
            body.nl()
            body.print(".replace(/[?&]$/, \"\")")
            body.unIndent()
            body.print ";"
            body.nl();

            // Body
            if op.RequestBody |> isNull |> not then
                body.print ("const content = JSON.stringify(request);")
                body.nl()

            body.print "const options: RequestInit = {"
            body.indent()
            body.nl()
            
            
            if op.RequestBody |> isNull |> not then
                body.print "body: content,"
                body.nl()
            body.print (sprintf "method: \"%s\"," (verb.ToString().ToUpper()))
            body.nl()
            body.print "headers: {"
            body.nl()
            body.indent()
            body.print "\"Content-Type\": \"application/json\","
            body.nl()
            body.print "\"Accept\": \"application/json\","
            body.nl()
            body.unIndent()
            body.print "},"
            body.unIndent()
            body.nl()
            body.print "};"
            body.nl()

            body.print "const response = await fetcher(url, options);"
            body.nl()

            body.print ("switch(response.status) {")
            body.nl()
            body.indent()
            let genByCode response =
                body.print(sprintf "case %d: {" response.Status);
                body.nl()
                body.indent()
                if response.IsVoid |> not then
                    body.print "const text = await response.text();"
                    body.nl()
                    body.print "const jsonAny = JSON.parse(text, reviver);"
                    body.nl()
                    if response.ReturnType.StartsWith("ValueTuple") then
                        body.print "const json = Object.entries(jsonAny)"
                        body.nl()
                        body.print "      .sort(([a], [b]) => (a.localeCompare(b)))"
                        body.nl()
                        body.print "      .map(([k, v]) => v);"
                        body.nl()
                    else
                        body.print "const json = jsonAny;"
                        body.nl()
                match responseCnts.[true], responseCnts.[false] with
                | 0, 0 -> body.print "return;"
                | 1, 0 -> if response.IsVoid then body.print "return;" else body.print "return json;"
                | 0, 1 -> if response.IsVoid then body.print "return;" else body.print "return json;"
                | 1, 1 ->
                    if response.IsVoid then
                        body.print (sprintf "return { type: \"%s\", data: undefined };"  (if response.Success then "success" else "failure"))
                    else
                        body.print (sprintf "return { type: \"%s\", data: (json as %s) };"  (if response.Success then "success" else "failure") response.ReturnType)
                | 1, _ ->
                    if response.Success then
                        if response.IsVoid then
                            body.print (sprintf "return { type: \"success\", data: undefined };")
                        else
                            body.print (sprintf "return { type: \"success\", data: (json as %s) };"  response.ReturnType)
                    else
                        if response.IsVoid then
                            body.print (sprintf "return { type: \"failure\", data: { status: %d, data: undefined } };"  response.Status)
                        else
                            body.print (sprintf "return { type: \"failure\", data: { status: %d, data: (json as %s) } };"  response.Status response.ReturnType)
                | _, 1 ->
                    if response.Success then
                        if response.IsVoid then
                            body.print (sprintf "return { type: \"success\", data: { status: %d, data: undefined  } };"  response.Status)
                        else
                            body.print (sprintf "return { type: \"success\", data: { status: %d, data: (json as %s) } };"  response.Status response.ReturnType)
                    else
                        if response.IsVoid then
                            body.print (sprintf "return { type: \"failure\", data: undefined };")
                        else
                            body.print (sprintf "return { type: \"failure\", data: (json as %s) };"  response.ReturnType)
                | _, _ ->
                    if response.IsVoid then
                        body.print (sprintf "return { type: \"%s\", data: { status: %d, data: undefined } };"
                            (if response.Success then "success" else "failure")  response.Status ) 
                    else
                        body.print (sprintf "return { type: \"%s\", data: { status: %d, data: (json as %s) } };"
                            (if response.Success then "success" else "failure")  response.Status response.ReturnType) 
                body.nl()
                body.unIndent();
                body.print("};")
                body.nl()
            responses
            |> Seq.iter genByCode
            body.unIndent();
            body.print("};")
            body.nl()

            body.print "throw new HttpError(response);"
            body.nl()

            body.nl()
            body.unIndent()
            body.print "},"
            body.nl()
        path.Operations |> Seq.iter(fun p -> generateOp p.Key p.Value)
    let paths = doc.Paths;
    intf.print "export interface ApiClient {"
    intf.nl()
    intf.indent()

    body.print "export const apiClientFactory = (url: string, fetcher: typeof fetch,"
    body.nl()
    body.print "       reviver: (key: string, value: any) => any = (key, value) => (value)) => ({"
    body.nl()
    body.indent()

    paths |> Seq.iter (fun p -> generatePath p.Key p.Value)

    body.unIndent()
    body.print "});"
    body.nl()

    intf.unIndent()
    intf.print "};"
    intf.nl()
    intf.nl();
    types.build() + intf.build() + body.build()

