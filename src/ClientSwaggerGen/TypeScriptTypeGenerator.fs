module TypeScriptTypeGenerator
open Microsoft.OpenApi.Models
open Robomed.ClientSwaggerGen.Patterns
open System.Text
open System
open System.Linq
open System.Runtime.InteropServices.ComTypes
open System.Collections.Generic
open System.Net
open Robomed.ClientSwaggerGen.SourceBuilder
open System.Reflection
open System.IO


let normalize (s :string) = s.Replace("[", "").Replace("]", "").Replace(",", "").Replace(" ", "")

let rec generateObject writer (schema : OpenApiSchema)  =
    let isRequired propName = schema.Required.Contains(propName)
    let genProperty pn sh =
        let isRequired = if isRequired pn then "" else "?"
        writer.print (sprintf "%s%s: " pn isRequired)
        writer.indent()
        generateInnerType writer sh 
        writer.unIndent()
        writer.print ";"
    writer.print "{"
    writer.nl()
    writer.indent()
    schema.Properties |> Seq.iter (fun p -> genProperty p.Key p.Value; writer.nl())
    writer.unIndent()
    writer.print "}"
    
and generateInnerType writer (schema : OpenApiSchema) =
    let nullable =
        if schema.Nullable then " | null" else ""
    
    match schema with
    | IsLocalRef ref -> writer.print (normalize ref)
    | IsInteger _ -> writer.print "number"
    | IsNumber _ -> writer.print "number"
    | IsBoolean _ -> writer.print "boolean"
    | StringEnum keys ->
        keys |> Seq.map (fun t -> "\"" + t + "\"") |> String.concat " | " |> writer.print
    | IsString pat ->
        match pat with
        | None -> "string" |> writer.print
        | Some p ->
            match p with
            | "date-time" -> "moment.Moment" |> writer.print
            | _ -> "string" |> writer.print
    | IsDictionary vType ->
        writer.print "{ [key: string]: "
        generateInnerType writer vType 
        writer.print " }"
    | IsObject -> generateObject writer schema 
    | Array el -> generateInnerType writer el ; writer.print "[]"
    | _ -> invalidOp "unknown schema"
    writer.print nullable

let generateType name (schema : OpenApiSchema) (all: IDictionary<string, OpenApiSchema>) writer  =
    match schema with
    | IsValueTuple name ->
        sprintf "export type %s = [ " (normalize name) |> writer.print
        schema.Properties.Values |> Seq.iteri (fun i sh ->
            if i > 0 then writer.print ", "
            generateInnerType writer sh) 
        " ];" |> writer.print
        writer.nl()
    | IsVersioned ->
        sprintf "export type %s = Versioned<" (normalize name) |> writer.print
        generateInnerType writer schema.Properties.["data"]
        ">;" |> writer.print
        writer.nl()
    | IsVersionedList ->
        sprintf "export type %s = VersionedList<" (normalize name) |> writer.print
        generateInnerType writer schema.Properties.["data"].Items
        ">;" |> writer.print
        writer.nl()
    | _ when IsAbstract schema && schema.Discriminator |> isNull |> not &&  schema.Discriminator.Mapping |> isNull |> not ->
        sprintf "export type %s = " (normalize name) |> writer.print
        schema.Discriminator.Mapping.Values |> Seq.iteri (fun i v ->
            if i > 0 then writer.print " | "
            writer.print v)
        writer.print ";"
        writer.nl()
    | IsUnionCase ->
        let union = all.[schema.AllOf.[0].Reference.Id]
        let dict = union.Discriminator.Mapping |> Seq.map (fun p -> p.Value, p.Key) |> Map.ofSeq
        sprintf "export interface %s extends %s {" (normalize name) (normalize schema.AllOf.[1].Reference.Id) |> writer.print
        writer.nl()
        writer.indent()
        sprintf "$type: \"%s\";"  dict.[name] |> writer.print
        writer.nl()
        writer.unIndent()
        sprintf "}" |> writer.print
        writer.nl()
    | IsObject ->
        sprintf "export interface %s " (normalize name) |> writer.print
        generateObject writer schema 
        writer.nl()
    writer.nl()
let generate (document : OpenApiDocument) =
    let commonTypes =
        use stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ClientSwaggerGen.CommonTypes.ts")
        use reader = new StreamReader(stream)
        reader.ReadToEnd()
    let writer = create 2
    writer.print commonTypes

    document.Components.Schemas 
        |> Seq.iter (fun p -> generateType p.Key p.Value document.Components.Schemas writer)
    writer.build()
