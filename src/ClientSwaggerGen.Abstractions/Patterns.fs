module Robomed.ClientSwaggerGen.Patterns

open System
open Microsoft.OpenApi.Models
open Microsoft.OpenApi.Any
open System.ComponentModel

let hasType<'t> (v:obj) = match v with | :? 't -> true | _ -> false


let (|Array|_|) (schema: OpenApiSchema) =
    if schema.Type = "array" then Some schema.Items else None

let (|StringEnum|_|) (schema: OpenApiSchema) =
    match schema.Enum with
    | null -> None
    | p when p.Count = 0 -> None
    | p when p |>  Seq.exists (hasType<OpenApiString> >> not) -> None
    | p -> p |> Seq.cast<OpenApiString> |> Seq.map (fun s -> s.Value) |> Seq.toList |> Some

let (|IsLocalRef|_|) (schema: OpenApiSchema) =
   match schema.Reference with
   | null -> None
   | p when p.IsLocal -> p.Id |> Some
   | _ -> None

let private hasSchemaType typ (schema : OpenApiSchema) = if schema.Type = typ then Some(schema.Format |> Option.ofObj) else None

let (|IsString|_|) schema = schema |> hasSchemaType "string"
let (|IsInteger|_|) schema  = schema |> hasSchemaType "integer"
let (|IsNumber|_|) schema = schema |> hasSchemaType "number"
let (|IsBoolean|_|) schema = schema |> hasSchemaType "boolean"
let (|IsObject|_|) (schema : OpenApiSchema) = if schema.Type = "object" then Some() else None 
let (|IsValueTuple|_|) (name:string) (schema : OpenApiSchema) =
    if schema.Type = "object" &&  name.StartsWith("ValueTuple[") then Some() else None
let IsAbstract (schema : OpenApiSchema) = schema.Extensions.ContainsKey("x-abstract")
let (|IsUnionCase|_|) (schema : OpenApiSchema) =
    if schema.AllOf |> isNull |> not && schema.AllOf.Count = 2 then Some() else None
let(|IsDictionary|_|) (schema : OpenApiSchema) =
    if schema.AdditionalPropertiesAllowed && schema.AdditionalProperties |> isNull |> not then
        Some(schema.AdditionalProperties)
    else None

let private isWellknown (schema:OpenApiSchema) str =
    if schema.Extensions.ContainsKey("x-wellknownType") then
        match schema.Extensions.["x-wellknownType"] with
        | :? OpenApiString as oas ->
            match oas.Value with
            | _ when oas.Value = str -> Some()
            | _ -> None
        | _ -> None
    else
        None
let (|IsVersioned|_|) schema = isWellknown schema "Versioned<T>"
let (|IsVersionedList|_|) schema = isWellknown schema "VersionedList<T>"
    
            
    