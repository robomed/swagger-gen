module Robomed.ClientSwaggerGen.SourceBuilder

open System
open System.Text

module Writer =
    type Writer = private {
        Builder : StringBuilder
        IndentValue: string
        Indent: uint16
        NeedIndent: bool
    }

    let create indent = {
        Builder = new StringBuilder()
        IndentValue = String.replicate indent " "
        Indent = 0us
        NeedIndent = true
    }
    
    let indent writer =
        { writer with Indent = writer.Indent + 1us }
    
    let unIndent writer =
        { writer with Indent = writer.Indent - 1us }
    
    let print (str: string) writer =
        if writer.NeedIndent then
            writer.Builder.Append(String.replicate (int32 writer.Indent) writer.IndentValue) |> ignore
        writer.Builder.Append(str) |> ignore
        { writer with NeedIndent = false }
    
    let nl writer =
        writer.Builder.AppendLine() |> ignore
        { writer with NeedIndent = true }
    
    let build writer =
        writer.Builder.ToString()

type WriterCommand =
    | Indent
    | Unindent
    | Print of string
    | NL
    | Build of AsyncReplyChannel<string>

type SourceBuilder = {
    indent: unit -> unit
    unIndent: unit -> unit
    print: string -> unit
    nl: unit -> unit
    build: unit -> string
}

let create indent =
    let writer = Writer.create indent
    let mailbox =MailboxProcessor.Start(fun inbox ->
        let rec loop writer =
            async {
                let! cmd = inbox.Receive()
                let next =
                    match cmd with
                    | Indent -> writer |> Writer.indent
                    | Unindent -> writer |> Writer.unIndent
                    | Print s -> writer |> Writer.print s
                    | NL -> writer |> Writer.nl
                    | Build ch ->
                        ch.Reply(writer |> Writer.build)
                        writer
                return! (loop next)
            }
        loop writer)
    {
        indent = fun () -> mailbox.Post(Indent)
        unIndent = fun () -> mailbox.Post(Unindent)
        print = fun str -> mailbox.Post(Print str)
        nl = fun () -> mailbox.Post(NL)
        build = fun () -> mailbox.PostAndReply(fun ch -> Build ch)
    }

    
    
        


    






